package foundation.e.privacymodules.trackers.data;

import android.content.Context;

import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import foundation.e.privacymodules.trackers.Tracker;
import kotlin.Pair;

public class StatsRepository {
    private static StatsRepository instance;

    private StatsDatabase database;

    private Consumer<Boolean> newDataCallback = null;

    private StatsRepository(Context context) {
        database = new StatsDatabase(context);
    }

    public static StatsRepository getInstance(Context context) {
        if (instance == null) {
            instance = new StatsRepository(context);
        }
        return instance;
    }

    public void setNewDataCallback(Consumer<Boolean> callback) {
        newDataCallback = callback;
    }

    public void logAccess(String trackerId, int appUid, boolean blocked) {
        database.logAccess(trackerId, appUid, blocked);
        if (newDataCallback != null) {
            newDataCallback.accept(true);
        }
    }

    public List<Pair<Integer, Integer>> getTrackersCallsOnPeriod(int periodsCount, TemporalUnit periodUnit) {
        return database.getTrackersCallsOnPeriod(periodsCount, periodUnit);
    }

    public int getActiveTrackersByPeriod(int periodsCount, TemporalUnit periodUnit) {
        return database.getActiveTrackersByPeriod(periodsCount, periodUnit);
    }

    public Map<Integer, Integer> getContactedTrackersCountByApp() {
        return database.getContactedTrackersCountByApp();
    }

    public int getContactedTrackersCount() {
        return database.getContactedTrackersCount();
    }

    public List<Tracker> getAllTrackersOfApp(int app_uid) {
        return database.getAllTrackersOfApp(app_uid);
    }

    public Map<Integer, Pair<Integer, Integer>> getCallsByApps(int periodCount, TemporalUnit periodUnit) {
        return database.getCallsByApps(periodCount, periodUnit);
    }

    public Pair<Integer, Integer> getCalls(int appUid, int periodCount, TemporalUnit periodUnit) {
        return database.getCalls(appUid, periodCount, periodUnit);
    }


        public int getMostLeakedApp(int periodCount, TemporalUnit periodUnit) {
        return database.getMostLeakedApp(periodCount, periodUnit);
    }
}