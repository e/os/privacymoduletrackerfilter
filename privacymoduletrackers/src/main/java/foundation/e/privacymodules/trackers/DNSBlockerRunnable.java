package foundation.e.privacymodules.trackers;

/*
 PersonalDNSFilter 1.5
 Copyright (C) 2017 Ingo Zenz
 Copyright (C) 2021 ECORP

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 */

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

import foundation.e.privacymodules.trackers.data.TrackersRepository;
import foundation.e.privacymodules.trackers.data.WhitelistRepository;


public class DNSBlockerRunnable implements Runnable {

	LocalServerSocket resolverReceiver;
	boolean stopped = false;
	private final TrackersLogger trackersLogger;
	private final TrackersRepository trackersRepository;
	private final WhitelistRepository whitelistRepository;

	private int eBrowserAppUid = -1;

	private final String TAG = DNSBlockerRunnable.class.getName();
	private static final String SOCKET_NAME = "foundation.e.advancedprivacy";


	public DNSBlockerRunnable(Context ct, TrackersLogger trackersLogger, TrackersRepository trackersRepository, WhitelistRepository whitelistRepository) {
		this.trackersLogger = trackersLogger;
		this.trackersRepository = trackersRepository;
		this.whitelistRepository = whitelistRepository;
		initEBrowserDoTFix(ct);
	}

	public synchronized void stop() {
		stopped = true;
		closeSocket();
	}

	private void closeSocket() {
		// Known bug and workaround that LocalServerSocket::close is not working well
		// https://issuetracker.google.com/issues/36945762
		if (resolverReceiver != null) {
			try {
				Os.shutdown(resolverReceiver.getFileDescriptor(), OsConstants.SHUT_RDWR);
				resolverReceiver.close();
				resolverReceiver = null;
			} catch (ErrnoException e) {
				if (e.errno != OsConstants.EBADF) {
					Log.w(TAG, "Socket already closed");
				} else {
					Log.e(TAG, "Exception: cannot close DNS port on stop" + SOCKET_NAME + "!", e);
				}
			} catch (Exception e) {
				Log.e(TAG, "Exception: cannot close DNS port on stop" + SOCKET_NAME + "!", e);
			}
		}
	}

	@Override
	public void run() {
		try {
			resolverReceiver = new LocalServerSocket(SOCKET_NAME);
		} catch (IOException eio) {
			Log.e(TAG, "Exception:Cannot open DNS port " + SOCKET_NAME + "!", eio);
			return;
		}
		Log.d(TAG, "DNSFilterProxy running on port " + SOCKET_NAME + "!");

		while (!stopped) {
			try {
				LocalSocket socket = resolverReceiver.accept();

				BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				String line = reader.readLine();
				String[] params = line.split(",");
				OutputStream output = socket.getOutputStream();
				PrintWriter writer = new PrintWriter(output, true);

				String domainName = params[0];
				int appUid = Integer.parseInt(params[1]);
				boolean shouldBlock = false;

				if (isEBrowserDoTBlockFix(appUid, domainName)) {
					shouldBlock = true;
				} else if (trackersRepository.isTracker(domainName)) {
					String trackerId = trackersRepository.getTrackerId(domainName);

					if (whitelistRepository.isBlockingEnabled() &&
						!whitelistRepository.isAppWhiteListed(appUid) &&
						!whitelistRepository.isTrackerWhiteListedForApp(trackerId, appUid)) {
							writer.println("block");
							shouldBlock = true;
					}
					trackersLogger.logAccess(trackerId, appUid, shouldBlock);
				}

				if (!shouldBlock) {
					writer.println("pass");
				}
				socket.close();
				// Printing bufferedreader data
			} catch (IOException e) {
				Log.w(TAG, "Exception while listening DNS resolver", e);
			}
		}
	}

	private void initEBrowserDoTFix(Context context) {
		try {
			eBrowserAppUid = context.getPackageManager().getApplicationInfo("foundation.e.browser", 0).uid;
		} catch (PackageManager.NameNotFoundException e) {
			Log.i(TAG, "no E Browser package found.");
		}
	}

	private boolean isEBrowserDoTBlockFix(int appUid, String hostname) {
		return appUid == eBrowserAppUid && "chrome.cloudflare-dns.com".equals(hostname);
	}
}
