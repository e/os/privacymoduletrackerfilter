/*
 Copyright (C) 2021 ECORP

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 */


package foundation.e.privacymodules.trackers.api;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import foundation.e.privacymodules.permissions.data.ApplicationDescription;
import foundation.e.privacymodules.trackers.IBlockTrackersPrivacyModule;
import foundation.e.privacymodules.trackers.Tracker;
import foundation.e.privacymodules.trackers.data.TrackersRepository;
import foundation.e.privacymodules.trackers.data.WhitelistRepository;

public class BlockTrackersPrivacyModule implements IBlockTrackersPrivacyModule {

    private final Context mContext;
    private List<Listener> mListeners = new ArrayList<>();
    private static BlockTrackersPrivacyModule sBlockTrackersPrivacyModule;

    private TrackersRepository trackersRepository;
    private WhitelistRepository whitelistRepository;

    public BlockTrackersPrivacyModule(Context ct) {
        mContext = ct;
        trackersRepository = TrackersRepository.getInstance();
        whitelistRepository = WhitelistRepository.getInstance(mContext);
    }

    public static BlockTrackersPrivacyModule getInstance(Context ct){
        if(sBlockTrackersPrivacyModule == null){
            sBlockTrackersPrivacyModule = new BlockTrackersPrivacyModule(ct);
        }
        return sBlockTrackersPrivacyModule;
    }

    @Override
    public void addListener(Listener listener) {
        mListeners.add(listener);
    }

    @Override
    public void clearListeners() {
        mListeners.clear();
    }

    @Override
    public void disableBlocking() {
        whitelistRepository.setBlockingEnabled(false);
        for(Listener listener:mListeners){
            listener.onBlockingToggle(false);
        }
    }

    @Override
    public void enableBlocking() {
        whitelistRepository.setBlockingEnabled(true);
        for(Listener listener:mListeners){
            listener.onBlockingToggle(true);
        }
    }

    @Override
    public List<Tracker> getWhiteList(int appUid) {
        List<Tracker> trackers = new ArrayList();
        for (String trackerId: whitelistRepository.getWhiteListForApp(appUid)) {
            trackers.add(trackersRepository.getTracker(trackerId));
        }
        return trackers;
    }

    @Override
    public List<Integer> getWhiteListedApp() {
        return whitelistRepository.getWhiteListedApp();
    }

    @Override
    public boolean isBlockingEnabled() {
        return whitelistRepository.isBlockingEnabled();
    }

    @Override
    public boolean isWhiteListEmpty() {
        return whitelistRepository.areWhiteListEmpty();
    }

    @Override
    public boolean isWhitelisted(int appUid) {
        return whitelistRepository.isAppWhiteListed(appUid);
    }

    @Override
    public void removeListener(Listener listener) {
        mListeners.remove(listener);
    }

    @Override
    public void setWhiteListed(Tracker tracker, int appUid, boolean isWhiteListed) {
        whitelistRepository.setWhiteListed(tracker, appUid, isWhiteListed);
    }

    @Override
    public void setWhiteListed(int appUid, boolean isWhiteListed) {
        whitelistRepository.setWhiteListed(appUid, isWhiteListed);
    }
}
