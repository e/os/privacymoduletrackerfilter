package foundation.e.privacymodules.trackers.data;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import foundation.e.privacymodules.trackers.Tracker;

public class WhitelistRepository {
    private static final String SHARED_PREFS_FILE = "trackers_whitelist.prefs";
    private static final String KEY_BLOKING_ENABLED = "blocking_enabled";
    private static final String KEY_APPS_WHITELIST = "apps_whitelist";
    private static final String KEY_APP_TRACKERS_WHITELIST_PREFIX = "app_trackers_whitelist_";
    private static WhitelistRepository instance;

    private boolean isBlockingEnabled = false;
    private Set<Integer> appsWhitelist;
    private Map<Integer, Set<String>> trackersWhitelistByApp = new HashMap();

    private SharedPreferences prefs;
    private WhitelistRepository(Context context) {
        prefs = context.getSharedPreferences(SHARED_PREFS_FILE, Context.MODE_PRIVATE);
        reloadCache();
    }

    public static WhitelistRepository getInstance(Context context) {
        if (instance == null) {
            instance = new WhitelistRepository(context);
        }
        return instance;
    }

    private void reloadCache() {
        isBlockingEnabled = prefs.getBoolean(KEY_BLOKING_ENABLED, false);
        reloadAppsWhiteList();
        reloadAllAppTrackersWhiteList();
    }

    private void reloadAppsWhiteList() {
        HashSet<Integer> appWhiteList = new HashSet();
        for (String appUid: prefs.getStringSet(KEY_APPS_WHITELIST, new HashSet<String>())) {
            try {
                appWhiteList.add(Integer.parseInt(appUid));
            } catch (Exception e) { }
        }
        this.appsWhitelist = appWhiteList;
    }

    private void reloadAppTrackersWhiteList(int appUid) {
        String key = buildAppTrackersKey(appUid);
        trackersWhitelistByApp.put(appUid, prefs.getStringSet(key, new HashSet<String>()));
    }

    private void reloadAllAppTrackersWhiteList() {
        trackersWhitelistByApp.clear();
        for (String key: prefs.getAll().keySet()) {
            if (key.startsWith(KEY_APP_TRACKERS_WHITELIST_PREFIX)) {
                int appUid = Integer.parseInt(key.substring(KEY_APP_TRACKERS_WHITELIST_PREFIX.length()));
                reloadAppTrackersWhiteList(appUid);
            }
        }
    }

    public boolean isBlockingEnabled() { return isBlockingEnabled; }

    public void setBlockingEnabled(boolean enabled) {
        prefs.edit().putBoolean(KEY_BLOKING_ENABLED, enabled).apply();
        isBlockingEnabled = enabled;
    }

    public void setWhiteListed(int appUid, boolean isWhiteListed) {
        Set<String> current = new HashSet(prefs.getStringSet(KEY_APPS_WHITELIST, new HashSet<String>()));
        if (isWhiteListed) {
            current.add("" + appUid);
        } else {
            current.remove("" + appUid);
        }

        prefs.edit().putStringSet(KEY_APPS_WHITELIST, current).commit();
        reloadAppsWhiteList();
    }

    private String buildAppTrackersKey(int appUid) {
        return KEY_APP_TRACKERS_WHITELIST_PREFIX + appUid;
    }

    public void setWhiteListed(Tracker tracker, int appUid, boolean isWhiteListed) {
        Set<String> trackers;
        if (trackersWhitelistByApp.containsKey(appUid)) {
            trackers = trackersWhitelistByApp.get(appUid);
        } else {
            trackers = new HashSet<String>();
            trackersWhitelistByApp.put(appUid, trackers);
        }
        if (isWhiteListed) {
            trackers.add(tracker.getId());
        } else {
            trackers.remove(tracker.getId());
        }

        prefs.edit().putStringSet(buildAppTrackersKey(appUid), trackers).commit();
    }

    public boolean isAppWhiteListed(int appUid) {
        return appsWhitelist.contains(appUid);
    }

    public boolean isTrackerWhiteListedForApp(String trackerId, int appUid) {
        return trackersWhitelistByApp.getOrDefault(appUid, new HashSet()).contains(trackerId);
    }

    public boolean areWhiteListEmpty() {
        boolean empty = true;
        for (Set<String> trackers: trackersWhitelistByApp.values()) {
            empty = trackers.isEmpty();
        }

        return appsWhitelist.isEmpty() && empty;
    }

    public List<Integer> getWhiteListedApp() {
        return new ArrayList(appsWhitelist);
    }

    public List<String> getWhiteListForApp(int appUid) {
        return new ArrayList(trackersWhitelistByApp.getOrDefault(appUid, new HashSet()));
    }
}
