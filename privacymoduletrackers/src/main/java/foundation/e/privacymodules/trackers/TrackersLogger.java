package foundation.e.privacymodules.trackers;

import android.content.Context;
import android.util.Log;

import java.util.concurrent.LinkedBlockingQueue;

import foundation.e.privacymodules.trackers.data.StatsRepository;


public class TrackersLogger {
    private static final String TAG = "TrackerModule";
    private StatsRepository statsRepository;

    private LinkedBlockingQueue<DetectedTracker> queue;
    private boolean stopped = false;


    public TrackersLogger(Context context) {
        statsRepository = StatsRepository.getInstance(context);
        queue = new LinkedBlockingQueue<DetectedTracker>();
        startWriteLogLoop();
    }

    public void stop() {
        stopped = true;
    }

    public void logAccess(String trackerId, int appId, boolean wasBlocked) {
        queue.offer(new DetectedTracker(trackerId, appId, wasBlocked));
    }

    private void startWriteLogLoop() {
        Runnable writeLogRunner = new Runnable() {
            @Override
            public void run() {
                while(!stopped) {
                    try {
                        logAccess(queue.take());
                    } catch (InterruptedException e) {
                        Log.e(TAG, "writeLogLoop detectedTrackersQueue.take() interrupted: ", e);
                    }
                }
            }
        };
        new Thread(writeLogRunner).start();
    }


    public void logAccess(DetectedTracker detectedTracker) {
        statsRepository.logAccess(detectedTracker.trackerId, detectedTracker.appUid, detectedTracker.wasBlocked);
    }

    private class DetectedTracker {
        String trackerId;
        int appUid;
        boolean wasBlocked;

        public DetectedTracker(String trackerId, int appUid, boolean wasBlocked) {
            this.trackerId = trackerId;
            this.appUid = appUid;
            this.wasBlocked = wasBlocked;
        }
    }
}
