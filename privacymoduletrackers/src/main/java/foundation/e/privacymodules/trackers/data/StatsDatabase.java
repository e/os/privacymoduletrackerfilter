/*
 Copyright (C) 2021 ECORP

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 */


package foundation.e.privacymodules.trackers.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import foundation.e.privacymodules.trackers.Tracker;
import kotlin.Pair;

public class StatsDatabase extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "TrackerFilterStats.db";
    private final Object lock = new Object();
    private TrackersRepository trackersRepository;

    public StatsDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        trackersRepository = TrackersRepository.getInstance();
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }


    public static class AppTrackerEntry implements BaseColumns {
        public static final String TABLE_NAME = "tracker_filter_stats";
        public static final String COLUMN_NAME_TIMESTAMP = "timestamp";
        public static final String COLUMN_NAME_TRACKER = "tracker";
        public static final String COLUMN_NAME_APP_UID = "app_uid";
        public static final String COLUMN_NAME_NUMBER_CONTACTED = "sum_contacted";
        public static final String COLUMN_NAME_NUMBER_BLOCKED = "sum_blocked";

    }

    String[] projection = {
            AppTrackerEntry.COLUMN_NAME_TIMESTAMP,
            AppTrackerEntry.COLUMN_NAME_APP_UID,
            AppTrackerEntry.COLUMN_NAME_TRACKER,
            AppTrackerEntry.COLUMN_NAME_NUMBER_CONTACTED,
            AppTrackerEntry.COLUMN_NAME_NUMBER_BLOCKED
    };

    private static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + AppTrackerEntry.TABLE_NAME + " (" +
                    AppTrackerEntry._ID + " INTEGER PRIMARY KEY," +
                    AppTrackerEntry.COLUMN_NAME_TIMESTAMP + " INTEGER,"+
                    AppTrackerEntry.COLUMN_NAME_APP_UID + " INTEGER," +
                    AppTrackerEntry.COLUMN_NAME_TRACKER + " TEXT," +
                    AppTrackerEntry.COLUMN_NAME_NUMBER_CONTACTED + " INTEGER," +
                    AppTrackerEntry.COLUMN_NAME_NUMBER_BLOCKED + " INTEGER)";

    private static final String PROJECTION_NAME_PERIOD = "period";
    private static final String PROJECTION_NAME_CONTACTED_SUM = "contactedsum";
    private static final String PROJECTION_NAME_BLOCKED_SUM = "blockedsum";
    private static final String PROJECTION_NAME_LEAKED_SUM = "leakedsum";
    private static final String PROJECTION_NAME_TRACKERS_COUNT = "trackerscount";

    private HashMap<String, Pair<Integer, Integer>> getCallsByPeriod(
        int periodsCount,
        TemporalUnit periodUnit,
        String sqlitePeriodFormat
    ) {
        synchronized (lock) {
            long minTimestamp = getPeriodStartTs(periodsCount, periodUnit);

            SQLiteDatabase db = getReadableDatabase();

            String selection = AppTrackerEntry.COLUMN_NAME_TIMESTAMP + " >= ?";
            String[] selectionArg = new String[]{"" + minTimestamp};
            String projection =
                    AppTrackerEntry.COLUMN_NAME_TIMESTAMP + ", " +
                            "STRFTIME('" + sqlitePeriodFormat + "', DATETIME(" + AppTrackerEntry.COLUMN_NAME_TIMESTAMP + ", 'unixepoch', 'localtime')) " + PROJECTION_NAME_PERIOD + "," +
                            "SUM(" + AppTrackerEntry.COLUMN_NAME_NUMBER_CONTACTED + ") " + PROJECTION_NAME_CONTACTED_SUM + "," +
                            "SUM(" + AppTrackerEntry.COLUMN_NAME_NUMBER_BLOCKED + ") " + PROJECTION_NAME_BLOCKED_SUM;
            Cursor cursor = db.rawQuery("SELECT " + projection + " FROM " + AppTrackerEntry.TABLE_NAME
                    + " WHERE " + selection +
                    " GROUP BY " + PROJECTION_NAME_PERIOD +
                    " ORDER BY " + AppTrackerEntry.COLUMN_NAME_TIMESTAMP + " DESC" +
                    " LIMIT " + periodsCount, selectionArg);

            HashMap<String, Pair<Integer, Integer>> callsByPeriod = new HashMap<>();
            while (cursor.moveToNext()) {
                int contacted = cursor.getInt(cursor.getColumnIndex(PROJECTION_NAME_CONTACTED_SUM));
                int blocked = cursor.getInt(cursor.getColumnIndex(PROJECTION_NAME_BLOCKED_SUM));

                callsByPeriod.put(
                        cursor.getString(cursor.getColumnIndex(PROJECTION_NAME_PERIOD)),
                        new Pair(blocked, contacted - blocked)
                );
            }

            cursor.close();
            db.close();

            return callsByPeriod;
        }
    }

    private List<Pair<Integer, Integer>> callsByPeriodToPeriodsList(
        Map<String, Pair<Integer, Integer>> callsByPeriod,
        int periodsCount,
        TemporalUnit periodUnit,
        String javaPeriodFormat
    ) {
        ZonedDateTime currentDate = ZonedDateTime.now().minus(periodsCount, periodUnit);
        DateTimeFormatter formater = DateTimeFormatter.ofPattern(javaPeriodFormat);

        List<Pair<Integer, Integer>> calls = new ArrayList(periodsCount);
        for (int i = 0; i < periodsCount; i++) {
            currentDate = currentDate.plus(1, periodUnit);

            String currentPeriod = formater.format(currentDate);
            calls.add(callsByPeriod.getOrDefault(currentPeriod, new Pair(0, 0)));
        }
        return calls;
    }

    public List<Pair<Integer, Integer>> getTrackersCallsOnPeriod(int periodsCount, TemporalUnit periodUnit) {
        String sqlitePeriodFormat = "%Y%m";
        String javaPeriodFormat = "yyyyMM";

        if (periodUnit == ChronoUnit.MONTHS) {
            sqlitePeriodFormat = "%Y%m";
            javaPeriodFormat = "yyyyMM";
        } else if (periodUnit == ChronoUnit.DAYS) {
            sqlitePeriodFormat = "%Y%m%d";
            javaPeriodFormat = "yyyyMMdd";
        } else if (periodUnit == ChronoUnit.HOURS) {
            sqlitePeriodFormat = "%Y%m%d%H";
            javaPeriodFormat = "yyyyMMddHH";
        }

        Map<String, Pair<Integer, Integer>> callsByPeriod = getCallsByPeriod(periodsCount, periodUnit, sqlitePeriodFormat);
        return callsByPeriodToPeriodsList(callsByPeriod, periodsCount, periodUnit, javaPeriodFormat);
    }



    public int getActiveTrackersByPeriod(int periodsCount, TemporalUnit periodUnit) {
        synchronized (lock) {
            long minTimestamp = getPeriodStartTs(periodsCount, periodUnit);


            SQLiteDatabase db = getWritableDatabase();

            String selection = AppTrackerEntry.COLUMN_NAME_TIMESTAMP + " >= ? AND " +
                    AppTrackerEntry.COLUMN_NAME_NUMBER_CONTACTED + " > " + AppTrackerEntry.COLUMN_NAME_NUMBER_BLOCKED;
            String[] selectionArg = new String[]{"" + minTimestamp};
            String projection =
                    "COUNT(DISTINCT " + AppTrackerEntry.COLUMN_NAME_TRACKER + ") " + PROJECTION_NAME_TRACKERS_COUNT;
            Cursor cursor = db.rawQuery("SELECT " + projection + " FROM " + AppTrackerEntry.TABLE_NAME
                    + " WHERE " + selection, selectionArg);

            int count = 0;

            if (cursor.moveToNext()) {
                count = cursor.getInt(0);
            }

            cursor.close();
            db.close();

            return count;
        }

    }

    public int getContactedTrackersCount() {
        synchronized (lock) {
            SQLiteDatabase db = getReadableDatabase();
            String projection =
                    "COUNT(DISTINCT " + AppTrackerEntry.COLUMN_NAME_TRACKER + ") " + PROJECTION_NAME_TRACKERS_COUNT;

            Cursor cursor = db.rawQuery(
                    "SELECT " + projection + " FROM " + AppTrackerEntry.TABLE_NAME,
                    new String[]{});

            int count = 0;

            if (cursor.moveToNext()) {
                count = cursor.getInt(0);
            }

            cursor.close();
            db.close();

            return count;
        }
    }


    public Map<Integer, Integer> getContactedTrackersCountByApp() {
        synchronized (lock) {
            SQLiteDatabase db = getReadableDatabase();
            String projection =
                    AppTrackerEntry.COLUMN_NAME_APP_UID + ", " +
                    "COUNT(DISTINCT " + AppTrackerEntry.COLUMN_NAME_TRACKER + ") " + PROJECTION_NAME_TRACKERS_COUNT;

            Cursor cursor = db.rawQuery(
                "SELECT " + projection + " FROM " + AppTrackerEntry.TABLE_NAME +
                " GROUP BY " + AppTrackerEntry.COLUMN_NAME_APP_UID,
                new String[]{});

            HashMap<Integer, Integer> countByApp = new HashMap();

            while (cursor.moveToNext()) {
                countByApp.put(
                        cursor.getInt(cursor.getColumnIndex(AppTrackerEntry.COLUMN_NAME_APP_UID)),
                        cursor.getInt(cursor.getColumnIndex(PROJECTION_NAME_TRACKERS_COUNT))
                );
            }

            cursor.close();
            db.close();

            return countByApp;
        }
    }

    public Map<Integer, Pair<Integer, Integer>> getCallsByApps(int periodCount, TemporalUnit periodUnit) {
        synchronized (lock) {
            long minTimestamp = getPeriodStartTs(periodCount, periodUnit);

            SQLiteDatabase db = getReadableDatabase();

            String selection = AppTrackerEntry.COLUMN_NAME_TIMESTAMP + " >= ?";
            String[] selectionArg = new String[]{"" + minTimestamp};
            String projection =
                    AppTrackerEntry.COLUMN_NAME_APP_UID + ", " +
                    "SUM(" + AppTrackerEntry.COLUMN_NAME_NUMBER_CONTACTED + ") " + PROJECTION_NAME_CONTACTED_SUM + "," +
                            "SUM(" + AppTrackerEntry.COLUMN_NAME_NUMBER_BLOCKED + ") " + PROJECTION_NAME_BLOCKED_SUM;

            Cursor cursor = db.rawQuery(
            "SELECT " + projection + " FROM " + AppTrackerEntry.TABLE_NAME +
                " WHERE " + selection +
                " GROUP BY " + AppTrackerEntry.COLUMN_NAME_APP_UID,
                selectionArg);


            HashMap<Integer, Pair<Integer, Integer>> callsByApp = new HashMap<>();

            while (cursor.moveToNext()) {
                int contacted = cursor.getInt(cursor.getColumnIndex(PROJECTION_NAME_CONTACTED_SUM));
                int blocked = cursor.getInt(cursor.getColumnIndex(PROJECTION_NAME_BLOCKED_SUM));

                callsByApp.put(
                    cursor.getInt(cursor.getColumnIndex(AppTrackerEntry.COLUMN_NAME_APP_UID)),
                    new Pair(blocked, contacted - blocked)
                );
            }

            cursor.close();
            db.close();

            return callsByApp;
        }
    }

    public Pair<Integer, Integer> getCalls(int appUid, int periodCount, TemporalUnit periodUnit) {
        synchronized (lock) {
            long minTimestamp = getPeriodStartTs(periodCount, periodUnit);

            SQLiteDatabase db = getReadableDatabase();

            String selection =
                    AppTrackerEntry.COLUMN_NAME_APP_UID + " = ? AND " +
                    AppTrackerEntry.COLUMN_NAME_TIMESTAMP + " >= ?";
            String[] selectionArg = new String[]{ "" + appUid, "" + minTimestamp };
            String projection =
                    "SUM(" + AppTrackerEntry.COLUMN_NAME_NUMBER_CONTACTED + ") " + PROJECTION_NAME_CONTACTED_SUM + "," +
                    "SUM(" + AppTrackerEntry.COLUMN_NAME_NUMBER_BLOCKED + ") " + PROJECTION_NAME_BLOCKED_SUM;

            Cursor cursor = db.rawQuery(
                "SELECT " + projection + " FROM " + AppTrackerEntry.TABLE_NAME +
                    " WHERE " + selection,
                    selectionArg);

            HashMap<Integer, Pair<Integer, Integer>> callsByApp = new HashMap<>();

            Pair<Integer, Integer> calls = new Pair(0, 0);

            if (cursor.moveToNext()) {
                int contacted = cursor.getInt(cursor.getColumnIndex(PROJECTION_NAME_CONTACTED_SUM));
                int blocked = cursor.getInt(cursor.getColumnIndex(PROJECTION_NAME_BLOCKED_SUM));

                calls = new Pair(blocked, contacted - blocked);
            }

            cursor.close();
            db.close();

            return calls;
        }
    }

    public int getMostLeakedApp(int periodCount, TemporalUnit periodUnit) {
        synchronized (lock) {
            long minTimestamp = getPeriodStartTs(periodCount, periodUnit);

            SQLiteDatabase db = getReadableDatabase();

            String selection = AppTrackerEntry.COLUMN_NAME_TIMESTAMP + " >= ?";
            String[] selectionArg = new String[]{"" + minTimestamp};
            String projection =
                    AppTrackerEntry.COLUMN_NAME_APP_UID + ", " +
                            "SUM(" + AppTrackerEntry.COLUMN_NAME_NUMBER_CONTACTED +
                                " - " + AppTrackerEntry.COLUMN_NAME_NUMBER_BLOCKED +
                            ") " + PROJECTION_NAME_LEAKED_SUM;

            Cursor cursor = db.rawQuery(
            "SELECT " + projection + " FROM " + AppTrackerEntry.TABLE_NAME +
                " WHERE " + selection +
                " GROUP BY " + AppTrackerEntry.COLUMN_NAME_APP_UID +
                " ORDER BY " + PROJECTION_NAME_LEAKED_SUM + " DESC LIMIT 1",
                selectionArg);


            int appUid = 0;
            if (cursor.moveToNext()) {
                appUid = cursor.getInt(cursor.getColumnIndex(AppTrackerEntry.COLUMN_NAME_APP_UID));
            }

            cursor.close();
            db.close();

            return appUid;
        }
    }

    private long getCurrentHourTs() {
        long hourInMs = TimeUnit.HOURS.toMillis(1L);
        long hourInS = TimeUnit.HOURS.toSeconds(1L);
        return (System.currentTimeMillis() / hourInMs) * hourInS;
    }

    private long getPeriodStartTs(
            int periodsCount,
            TemporalUnit periodUnit
    ) {

        ZonedDateTime start = ZonedDateTime.now()
                .minus(periodsCount, periodUnit)
                .plus(1, periodUnit);

        TemporalUnit truncatePeriodUnit = periodUnit;
        if (periodUnit == ChronoUnit.MONTHS) {
            start = start.withDayOfMonth(1);
            truncatePeriodUnit = ChronoUnit.DAYS;
        }

        return start.truncatedTo(truncatePeriodUnit).toEpochSecond();
    }

    public void logAccess(String trackerId, int appUid, boolean blocked){
        synchronized (lock) {
            long currentHour = getCurrentHourTs();
            SQLiteDatabase db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(AppTrackerEntry.COLUMN_NAME_APP_UID, appUid);
            values.put(AppTrackerEntry.COLUMN_NAME_TRACKER, trackerId);
            values.put(AppTrackerEntry.COLUMN_NAME_TIMESTAMP, currentHour);

        /*String query = "UPDATE product SET "+AppTrackerEntry.COLUMN_NAME_NUMBER_CONTACTED+" = "+AppTrackerEntry.COLUMN_NAME_NUMBER_CONTACTED+" + 1 ";
        if(blocked)
            query+=AppTrackerEntry.COLUMN_NAME_NUMBER_BLOCKED+" = "+AppTrackerEntry.COLUMN_NAME_NUMBER_BLOCKED+" + 1 ";
*/
            String selection =
                    AppTrackerEntry.COLUMN_NAME_TIMESTAMP + " = ? AND " +
                    AppTrackerEntry.COLUMN_NAME_APP_UID + " = ? AND " +
                    AppTrackerEntry.COLUMN_NAME_TRACKER + " = ? ";

            String[] selectionArg = new String[]{"" + currentHour, "" + appUid, trackerId};

            Cursor cursor = db.query(
                    AppTrackerEntry.TABLE_NAME,
                    projection,
                    selection,
                    selectionArg,
                    null,
                    null,
                    null
            );
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                StatEntry entry = cursorToEntry(cursor);
                if (blocked)
                    values.put(AppTrackerEntry.COLUMN_NAME_NUMBER_BLOCKED, entry.sum_blocked + 1);
                else
                    values.put(AppTrackerEntry.COLUMN_NAME_NUMBER_BLOCKED, entry.sum_blocked);
                values.put(AppTrackerEntry.COLUMN_NAME_NUMBER_CONTACTED, entry.sum_contacted + 1);
                db.update(AppTrackerEntry.TABLE_NAME, values, selection, selectionArg);

                // db.execSQL(query, new String[]{""+hour, ""+day, ""+month, ""+year, ""+appUid, ""+trackerId});
            } else {

                if (blocked)
                    values.put(AppTrackerEntry.COLUMN_NAME_NUMBER_BLOCKED, 1);
                else
                    values.put(AppTrackerEntry.COLUMN_NAME_NUMBER_BLOCKED, 0);
                values.put(AppTrackerEntry.COLUMN_NAME_NUMBER_CONTACTED, 1);


                long newRowId = db.insert(AppTrackerEntry.TABLE_NAME, null, values);
            }

            cursor.close();
            db.close();
        }
    }


    private StatEntry cursorToEntry(Cursor cursor){
        StatEntry entry = new StatEntry();
        entry.timestamp = cursor.getLong(cursor.getColumnIndex(AppTrackerEntry.COLUMN_NAME_TIMESTAMP));
        entry.app_uid = cursor.getInt(cursor.getColumnIndex(AppTrackerEntry.COLUMN_NAME_APP_UID));
        entry.sum_blocked = cursor.getInt(cursor.getColumnIndex(AppTrackerEntry.COLUMN_NAME_NUMBER_BLOCKED));
        entry.sum_contacted = cursor.getInt(cursor.getColumnIndex(AppTrackerEntry.COLUMN_NAME_NUMBER_CONTACTED));
        entry.tracker = cursor.getInt(cursor.getColumnIndex(AppTrackerEntry.COLUMN_NAME_TRACKER));
        return entry;
    }

    public List<Tracker> getAllTrackersOfApp(int appUid){
        synchronized (lock) {
            String[] columns = { AppTrackerEntry.COLUMN_NAME_TRACKER, AppTrackerEntry.COLUMN_NAME_APP_UID };
            String selection = null;
            String[] selectionArg = null;
            if (appUid >= 0) {
                selection = AppTrackerEntry.COLUMN_NAME_APP_UID + " = ?";
                selectionArg = new String[]{"" + appUid};
            }
            SQLiteDatabase db = getReadableDatabase();
            Cursor cursor = db.query(
                    true,
                    AppTrackerEntry.TABLE_NAME,
                    columns,
                    selection,
                    selectionArg,
                    null,
                    null,
                    null,
                    null
            );
            List<Tracker> trackers = new ArrayList<>();

            while (cursor.moveToNext()) {
                String trackerId = cursor.getString(cursor.getColumnIndex(AppTrackerEntry.COLUMN_NAME_TRACKER));
                Tracker tracker = trackersRepository.getTracker(trackerId);

                if (tracker != null) {
                    trackers.add(tracker);
                }
            }
            cursor.close();
            db.close();
            return trackers;
        }
    }

    public List<Tracker> getAllTrackers(){
        return getAllTrackersOfApp(-1);
    }

    public static class StatEntry {
        int app_uid;
        int sum_contacted;
        int sum_blocked;
        long timestamp;
        int tracker;
    }
}
