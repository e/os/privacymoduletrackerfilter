package foundation.e.privacymodules.trackers.data;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import foundation.e.privacymodules.trackers.Tracker;

public class TrackersRepository {
    private static TrackersRepository instance;

    private TrackersRepository() { }

    public static TrackersRepository getInstance() {
        if (instance == null) {
            instance = new TrackersRepository();
        }
        return instance;
    }

    private Map<String, Tracker> trackersById = new HashMap();
    private Map<String, String> hostnameToId = new HashMap();

    public void setTrackersList(List<Tracker> list) {
        Map<String, Tracker> trackersById = new HashMap();
        Map<String, String> hostnameToId = new HashMap();

        for (Tracker tracker: list) {
            trackersById.put(tracker.getId(), tracker);

            for (String hostname: tracker.getHostnames()) {
                hostnameToId.put(hostname, tracker.getId());
            }
        }

        this.trackersById = trackersById;
        this.hostnameToId = hostnameToId;
    }

    public boolean isTracker(String hostname) {
        return hostnameToId.containsKey(hostname);
    }

    public String getTrackerId(String hostname) {
        return hostnameToId.get(hostname);
    }

    public Tracker getTracker(String id) {
        return trackersById.get(id);
    }
}
