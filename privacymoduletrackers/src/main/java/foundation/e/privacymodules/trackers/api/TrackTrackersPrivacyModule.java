/*
 Copyright (C) 2021 ECORP

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 */


package foundation.e.privacymodules.trackers.api;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import foundation.e.privacymodules.trackers.DNSBlockerService;
import foundation.e.privacymodules.trackers.ITrackTrackersPrivacyModule;
import foundation.e.privacymodules.trackers.Tracker;
import foundation.e.privacymodules.trackers.data.StatsRepository;
import foundation.e.privacymodules.trackers.data.TrackersRepository;
import kotlin.Pair;

public class TrackTrackersPrivacyModule implements ITrackTrackersPrivacyModule {

    private static TrackTrackersPrivacyModule sTrackTrackersPrivacyModule;
    private final Context mContext;
    private StatsRepository statsRepository;
    private List<ITrackTrackersPrivacyModule.Listener> mListeners = new ArrayList();

    public TrackTrackersPrivacyModule(Context ct) {
        mContext = ct;
        statsRepository = StatsRepository.getInstance(ct);
        statsRepository.setNewDataCallback((newData) -> {
            mListeners.forEach((listener) -> { listener.onNewData(); });
        });
    }

    public static TrackTrackersPrivacyModule getInstance(Context ct){
        if(sTrackTrackersPrivacyModule == null){
            sTrackTrackersPrivacyModule = new TrackTrackersPrivacyModule(ct);
        }
        return sTrackTrackersPrivacyModule;
    }

    public void start(List<Tracker> trackers, boolean enableNotification) {
        TrackersRepository.getInstance().setTrackersList(trackers);

        Intent intent = new Intent(mContext, DNSBlockerService.class);
        intent.setAction(DNSBlockerService.ACTION_START);
        intent.putExtra(DNSBlockerService.EXTRA_ENABLE_NOTIFICATION, enableNotification);
        mContext.startService(intent);
    }

    @NonNull
    @Override
    public List<Pair<Integer, Integer>> getPastDayTrackersCalls() {
        return statsRepository.getTrackersCallsOnPeriod(24, ChronoUnit.HOURS);
    }

    @Override
    public List<Pair<Integer, Integer>> getPastMonthTrackersCalls() {
        return statsRepository.getTrackersCallsOnPeriod(30, ChronoUnit.DAYS);
    }

    @Override
    public List<Pair<Integer, Integer>> getPastYearTrackersCalls() {
        return statsRepository.getTrackersCallsOnPeriod(12, ChronoUnit.MONTHS);
    }

    @Override
    public int getTrackersCount() {
        return statsRepository.getContactedTrackersCount();
    }

    @Override
    public Map<Integer, Integer> getTrackersCountByApp() {
        return statsRepository.getContactedTrackersCountByApp();
    }

    @Override
    public List<Tracker> getTrackersForApp(int i) {
        return statsRepository.getAllTrackersOfApp(i);
    }


    @Override
    public int getPastDayTrackersCount() {
        return statsRepository.getActiveTrackersByPeriod(24, ChronoUnit.HOURS);
    }

    @Override
    public int getPastMonthTrackersCount() {
        return statsRepository.getActiveTrackersByPeriod(30, ChronoUnit.DAYS);
    }

    @Override
    public int getPastYearTrackersCount() {
        return statsRepository.getActiveTrackersByPeriod(12, ChronoUnit.MONTHS);
    }

    @Override
    public int getPastDayMostLeakedApp() {
        return statsRepository.getMostLeakedApp(24, ChronoUnit.HOURS);
    }

    @NonNull
    @Override
    public Map<Integer, Pair<Integer, Integer>> getPastDayTrackersCallsByApps() {
        return statsRepository.getCallsByApps(24, ChronoUnit.HOURS);
    }

    @NonNull
    @Override
    public Pair<Integer, Integer> getPastDayTrackersCallsForApp(int appUid) {
        return statsRepository.getCalls(appUid, 24, ChronoUnit.HOURS);
    }

    @Override
    public void addListener(ITrackTrackersPrivacyModule.Listener listener) {
        mListeners.add(listener);
    }

    @Override
    public void removeListener(ITrackTrackersPrivacyModule.Listener listener) {
        mListeners.remove(listener);
    }

    @Override
    public void clearListeners() {
        mListeners.clear();
    }
}
