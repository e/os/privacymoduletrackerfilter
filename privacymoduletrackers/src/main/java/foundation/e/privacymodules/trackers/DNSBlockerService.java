/*
 Copyright (C) 2021 ECORP

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 2
 of the License, or (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

 */


package foundation.e.privacymodules.trackers;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import foundation.e.privacymodules.trackers.data.TrackersRepository;
import foundation.e.privacymodules.trackers.data.WhitelistRepository;

public class DNSBlockerService extends Service {
    private static final String TAG = "DNSBlockerService";
    private static DNSBlockerRunnable sDNSBlocker;
    private TrackersLogger trackersLogger;

    public static final String ACTION_START = "foundation.e.privacymodules.trackers.intent.action.START";

    public static final String EXTRA_ENABLE_NOTIFICATION = "foundation.e.privacymodules.trackers.intent.extra.ENABLED_NOTIFICATION";

    public DNSBlockerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getBooleanExtra(EXTRA_ENABLE_NOTIFICATION, true)) {
            ForegroundStarter.startForeground(this);
        }

        if (intent != null && ACTION_START.equals(intent.getAction())) {
            stop();
            start();
        }

        return START_STICKY;
    }

    private void start() {
        try {
            trackersLogger = new TrackersLogger(this);
            sDNSBlocker = new DNSBlockerRunnable(this, trackersLogger,
                    TrackersRepository.getInstance(), WhitelistRepository.getInstance(this));

            new Thread(sDNSBlocker).start();
        } catch(Exception e) {
            Log.e(TAG, "Error while starting DNSBlocker service", e);
            stop();
        }
    }

    private void stop() {
        if (sDNSBlocker != null) {
            sDNSBlocker.stop();
        }
        sDNSBlocker = null;
        if (trackersLogger != null) {
            trackersLogger.stop();
        }
        trackersLogger = null;
    }
}
