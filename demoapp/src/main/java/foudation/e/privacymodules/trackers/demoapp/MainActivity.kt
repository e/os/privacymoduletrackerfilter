package foudation.e.privacymodules.trackers.demoapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import foundation.e.privacymodules.trackers.ITrackTrackersPrivacyModule
import foundation.e.privacymodules.trackers.Tracker
import foundation.e.privacymodules.trackers.api.BlockTrackersPrivacyModule
import foundation.e.privacymodules.trackers.api.TrackTrackersPrivacyModule
import java.io.InputStreamReader
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private val appUid = 10141
    private var mBtnStart: Button? = null
    private var mBtnYearsLeaks: Button? = null
    private var mBtnMonthsLeaks: Button? = null
    private var mBtnDaysLeaks: Button? = null
    private var mBtnYearsTrackers: Button? = null
    private var mBtnMonthsTrackers: Button? = null
    private var mBtnDaysTrackers: Button? = null
    private var mBtnAllTrackers: Button? = null
    private var mBtnTrackersAppUid: Button? = null
    private var mBtnWhitelistApp: Button? = null
    private var mBtnWhitelistTracker: Button? = null
    private var mBtnShowWhitelist: Button? = null

    private var mTvLog: TextView? = null
    private var trackers = emptyList<Tracker>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initFromAssets()

        mBtnStart = findViewById(R.id.button_start)
        mBtnYearsLeaks = findViewById(R.id.button_years_leaks)
        mBtnMonthsLeaks = findViewById(R.id.button_months_leaks)
        mBtnDaysLeaks = findViewById(R.id.button_days_leaks)
        mBtnYearsTrackers = findViewById(R.id.button_years_trackers)
        mBtnMonthsTrackers = findViewById(R.id.button_months_trackers)
        mBtnDaysTrackers = findViewById(R.id.button_days_trackers)
        mBtnAllTrackers = findViewById(R.id.button_all_trackers)
        mBtnTrackersAppUid = findViewById(R.id.button_trackers_appuid)
        mBtnWhitelistApp = findViewById(R.id.button_whitelist_app)
        mBtnWhitelistTracker = findViewById(R.id.button_whitelist_tracker)
        mBtnShowWhitelist = findViewById(R.id.button_show_whitelist)
        mTvLog = findViewById(R.id.tv_log)

        val trackTrackersPrivacyModule = TrackTrackersPrivacyModule.getInstance(applicationContext)
        val blockTrackersPrivacyModule = BlockTrackersPrivacyModule.getInstance(applicationContext)

        mBtnStart?.setOnClickListener { v: View? ->
            trackTrackersPrivacyModule.start(trackers,
//            listOf(
//                Tracker(
//                    id = "1",
//                    hostnames = setOf("lemonde.fr"),
//                    label = "Test 1",
//                    description = null,
//                    website = null)
//            ),
            true);
        }

        mBtnYearsLeaks?.setOnClickListener { v: View? ->
            mTvLog?.append("Years leaks: ${trackTrackersPrivacyModule.getPastYearTrackersCalls()}")
        }

        mBtnMonthsLeaks?.setOnClickListener { v: View? ->
            mTvLog?.append("Months leaks: ${trackTrackersPrivacyModule.getPastMonthTrackersCalls()}")
        }

        mBtnDaysLeaks?.setOnClickListener { v: View? ->
            mTvLog?.append("Days leaks: ${trackTrackersPrivacyModule.getPastDayTrackersCalls()}")
        }

        mBtnYearsTrackers?.setOnClickListener { v: View? ->
            mTvLog?.append("Years trackers: ${trackTrackersPrivacyModule.getPastYearTrackersCount()}")
        }

        mBtnMonthsTrackers?.setOnClickListener { v: View? ->
            mTvLog?.append("Months trackers: ${trackTrackersPrivacyModule.getPastMonthTrackersCount()}")
        }

        mBtnDaysTrackers?.setOnClickListener { v: View? ->
            mTvLog?.append("Days trackers: ${trackTrackersPrivacyModule.getPastDayTrackersCount()}")
        }

        mBtnAllTrackers?.setOnClickListener { v: View? ->
            mTvLog?.append("All trackers count: ${trackTrackersPrivacyModule.getTrackersCount()}")
            mTvLog?.append("All trackers count by appUid:\n ${trackTrackersPrivacyModule.getTrackersCountByApp().entries.joinToString("\n") { "App: ${it.key} has ${it.value} trackers"}}")
        }

        mBtnTrackersAppUid?.setOnClickListener { v: View? ->
            mTvLog?.append("Trackers of appUid: ${trackTrackersPrivacyModule.getTrackersForApp(appUid).map {it.label}.joinToString("; ")}")
        }

        mBtnWhitelistApp?.setOnClickListener {
            blockTrackersPrivacyModule.enableBlocking()
            blockTrackersPrivacyModule.setWhiteListed(appUid, !blockTrackersPrivacyModule.isWhitelisted(appUid))
        }

        mBtnWhitelistTracker?.setOnClickListener {
            blockTrackersPrivacyModule.enableBlocking()

            trackers.find { it.label == "OutBrain" }?.let {
                val isWhitelisted = blockTrackersPrivacyModule.getWhiteList(appUid).contains(it)
                blockTrackersPrivacyModule.setWhiteListed(it, appUid, !isWhitelisted)

            mTvLog?.append("Whitelisted ${!isWhitelisted}: ${it.id}")
        }
        }

        mBtnShowWhitelist?.setOnClickListener {
            mTvLog?.append("Whitelisted app: ${blockTrackersPrivacyModule.getWhiteListedApp().joinToString(" ; ")}")
            mTvLog?.append("Whitelisted trackers for $appUid: ${blockTrackersPrivacyModule.getWhiteList(appUid).map { it?.label }.joinToString(" ; ")}")
        }


        trackTrackersPrivacyModule.addListener(object: ITrackTrackersPrivacyModule.Listener {
            override fun onNewData() {

                //mTvLog?.append("New data!")
                //mTvLog?.text = "Trackers: ${trackTrackersPrivacyModule.getPastDayTrackersCount()}"
            }
        })
    }

    private fun initFromAssets() {
        try {
            val reader = InputStreamReader(getAssets().open("e_trackers.json"), "UTF-8")

            val trackerResponse =
                Gson().fromJson(reader, ETrackersResponse::class.java)

            trackers = mapper(trackerResponse)
        } catch(e: Exception) {
            Log.e("TrackersRepository", "While parsing trackers in assets", e)
        }
    }
    private fun mapper(response: ETrackersResponse): List<Tracker> {
        return response.trackers.mapNotNull {
            try {
                it.toTracker()
            } catch (e: Exception) {
                null
            }
        }
    }

    fun ETrackersResponse.ETracker.toTracker(): Tracker {
        return Tracker(
            id = id!!,
            hostnames = hostnames!!.toSet(),
            label = name!!,
            exodusId = exodusId
        )
    }

    data class ETrackersResponse(val trackers: List<ETracker>) {
        data class ETracker(
            val id: String?,
            val hostnames: List<String>?,
            val name: String?,
            val exodusId: String?

//            val description: String?,
//            @SerializedName("creation_date") val creationDate: String?,
//            @SerializedName("code_signature") val codeSignature: String?,
//            @SerializedName("network_signature") val networkSignature: String?,
//            val website: String?,
//            val categories: List<String>?,
        )
    }

}